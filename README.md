Queen Mary University of London

QM+ Activities reporting plugin

Vasileios Sotiras v.sotiras@qmul.ac.uk

© 2017

This local plugin creates menus for showing calendar activities based on 
these Moodle modules that contain DUE Dates: 

'assign'       
'kalvidassign' 
'choice'       
'choicegroup'  
'quiz'         
'feedback'     
'scorm'        
'hotpot'       
'glossary'     
'oublog'       
'forum'        
'forumng'      
'data'         
'ouwiki'       
'questionnaire'
'workshop'     
'adobeconnect' 
'lesson'       

Calendars are produced for Schools, Course Categories, Courses, Teachers and Students.

Calendars can be exported in calendar ICS format.

In order to access the Calendar Activities menu,
in your home page add an HTML Block
and add a link to the /local/qm_activities/index.php file.

User permissions and access on reporting is account based,
in order to protect personal data.
